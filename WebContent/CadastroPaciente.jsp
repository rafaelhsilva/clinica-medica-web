<%@page import="factory.PacienteFactory"%>
<%@page import="to.*"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="libs/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href=libs/bootstrap/dist/css/bootstrap.min.css />
	<link rel="stylesheet" type="text/css" href=libs/project-files/css/commom-style.css />

</script>
<title>Paciente</title>
</head>
<body>
	<%
	PacienteFactory pac = new  PacienteFactory();
	PacienteTO paciente= new PacienteTO();
	
	%> 
	<form action="PacienteServlet" class="form-inline" method="doGet">
		<table>
			<tr>
				<td>Nome:</td>
				<td><input type="text" name="nomePaciente" id="nomePaciente"/></td>
			</tr>
			<tr>
				<td>CPF:</td>
				<td><input type="text" name="cpfPaciente" id="cpfPaciente" /></td>
			</tr>
			<tr>
				<td>RG:</td>
				<td><input type="text" name=rgPaciente id="rgPaciente" /></td>
			</tr>				
			<tr>
				<td>CEP:</td>
				<td><input type="text" name="cepPaciente" id="cepPaciente" /></td>
			</tr>
			<tr>
				<td>Numero:</td>
				<td><input  type="text" name="numPaciente" id="numPaciente"/></td>
			</tr>	
			<tr>
				<td>Complemento:</td>
				<td><input type="text" name="complPaciente" id="complPaciente"/></td>
			</tr>
			<tr>
				<td>Tipo de Telefone:</td>
				<td>
				<select name="tipoTelPaciente" id="tipoTelPaciente">
				<%			
				List<TipoTelefoneTO> list = pac.getTiposTelefone();
				
				for(int i=0;i<list.size();i++){
					TipoTelefoneTO tp = list.get(i);
				%>  
					<option value=<%=tp.getId()%>> <%=tp.getNome()%>  </option> 
				<% } 
				%> 
				
				
				</select></td>
			</tr>	
			<tr>
				<td>Telefone:</td>
				<td>
					<input type="text" style="width: 40px;" maxlength="2" name="areaTelPaciente" id="areaTelPaciente"/>
					<input type="text" name="telPaciente" id="telPaciente"/>
				</td>
			</tr>											
		</table>
			<input type="submit" value="Cadastrar"/>
			<input type="button" value="Voltar" onclick="location.href='ListPaciente.jsp'"/>
	</form>
</body>
</html>