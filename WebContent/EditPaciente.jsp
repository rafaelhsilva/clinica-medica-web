<%@page import="factory.PacienteFactory"%>
<%@page import="to.*"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="libs/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href=libs/bootstrap/dist/css/bootstrap.min.css />
	<link rel="stylesheet" type="text/css" href=libs/project-files/css/commom-style.css />

<title>Edit</title>
</head>
<body>
	<%
	PacienteFactory pac = new  PacienteFactory();
	PacienteTO paciente= new PacienteTO();
	if (request.getParameter("pacienteId") != null) {
		int idLogin = Integer.valueOf(request.getParameter("pacienteId"));
		paciente = pac.getById(idLogin);

	}
	
	%> 
	<form action="PacienteServlet" class="form-inline" method="doGet">
			<input type="hidden" name="idPaciente" id="idPaciente" value="<%=paciente.getId()%>"/>
		<table>
			<tr>
				<td>Nome:</td>
				<td><input type="text" name="nomePaciente" id="nomePaciente" value="<%=paciente.getNome()%>"/></td>
			</tr>
			<tr>
				<td>CEP:</td>
				<td><input type="text" name="cepPaciente" id="cepPaciente" value="<%=paciente.getCep()%>"/></td>
			</tr>
			<tr>
				<td>Numero:</td>
				<td><input  type="text" name="numPaciente" id="numPaciente" value="<%=paciente.getNumero()%>"/></td>
			</tr>	
			<tr>
				<td>Complemento:</td>
				<td><input type="text" name="complPaciente" id="complPaciente" value="<%=paciente.getComplemento()%>"/></td>
			</tr>
			<tr>
				<td>CPF:</td>
				<td><input type="text" name="cpfPaciente" id="cpfPaciente" value="<%=paciente.getCpf()%>"/></td>
			</tr>
			<tr>
				<td>RG:</td>
				<td><input type="text" name=rgPaciente id="rgPaciente" value="<%=paciente.getRg()%>"/></td>
			</tr>
			<tr>
				<td>Login:</td>
				<td><input type="text" name="loginPaciente" id="loginPaciente" value="<%=paciente.getAcesso().getLogin()%>"/></td>
			</tr>
			<tr>
				<td>Senha:</td>
				<td><input type="password" name="senhaPaciente" id="senhaPaciente" value="<%=paciente.getAcesso().getSenha()%>"/></td>
			</tr>									
			<tr>
				<td>Tipo de Telefone:</td>
				<td>
				<select name="tipoTelPaciente" id="tipoTelPaciente">
				<%			
				List<TipoTelefoneTO> list = pac.getTiposTelefone();
				
				for(int i=0;i<list.size();i++){
					TipoTelefoneTO tp = list.get(i);
				%>  
					<option value=<%=tp.getId()%>> <%=tp.getNome()%>  </option> 
				<% } 
				%> 
				
				
				</select></td>
			</tr>	
			<tr>
				<td>Telefone:</td>
				<td><input type="text" style="width: 40px;" maxlength="2" name="areaTelPaciente" id="areaTelPaciente"value="<%=paciente.getTelefone().getArea()%>"/>
				<input type="text" name="telPaciente" id="telPaciente" value="<%=paciente.getTelefone().getNumero() %>"/></td>
			</tr>											
		</table>
			<input type="submit" value="Atualizar"/>
	</form>
</body>
</html>