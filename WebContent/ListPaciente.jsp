<%@page import="factory.PacienteFactory"%>
<%@page import="to.PacienteTO"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cadastro</title>
</head>
<body>
		<table>
			<th> Nome </th>
			<th> CEP </th>
			<th> Numero </th>
			<th> Login </th>
			<th> Ações </th>
			<%
				PacienteFactory pac = new  PacienteFactory();
				List<PacienteTO> list = new ArrayList<PacienteTO>();
				if (session.getAttribute("pacienteId") != null) {
					Integer id = Integer.parseInt(session.getAttribute("pacienteId").toString());
					list.add(pac.getById(id));
				} else {
					response.sendRedirect("Login.jsp");
				}
				
				for(int i=0;i<list.size();i++){
					PacienteTO p = list.get(i);
				%>  
				<tr>
					<td><%=p.getNome()%></td>
					<td><%=p.getCep() %> </td>
					<td><%=p.getNumero() %> </td>
					<td><%=p.getAcesso().getLogin() %> </td>
					<td>
						<a href="EditPaciente.jsp?pacienteId=<%=p.getId()%>">Alterar</a> 
						<form  id="form1" method="post" action="Excluir">
							<input type="submit" value="Excluir">
						</form> 
					
					</td>
				</tr>				
					
				<% } 
				%> 

		
		</table>
</body>
</html>