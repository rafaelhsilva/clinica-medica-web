<%@page import="factory.PacienteFactory"%>
<%@page import="java.util.*"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="libs/jquery/dist/jquery.min.js"></script>
<script type="text/javascript"
	src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href=libs/bootstrap/dist/css/bootstrap.min.css />
<link rel="stylesheet" type="text/css"
	href=libs/project-files/css/login-style.css />
<link rel="stylesheet" type="text/css"
	href=libs/project-files/css/commom-style.css />
<title>Login</title>
</head>
<body>
	<form action="AcessoServlet" class="form-inline form-login" method="doPost">
		<div class="vertical-center">
			<div class="container">
				<div class="row form-group login-content">
					<div>
						<label for="login" class="control-label"><span
							class="glyphicon glyphicon-user"></span></label> <input
							class="form-control" type="text" name="login" id="login" /><br>


						<label for="password" class=><span
							class="glyphicon glyphicon-lock"></span></label> <input
							class="form-control" type="password" name="password"
							id="password" /><br>
						<input class="btn btn-default margin-medium" type="submit" value="login" />
						<input type="button" class="btn btn-default margin-medium" value="Cadastre-se" onclick="javascript:window.location.href='CadastroPaciente.jsp'"/>
					</div>
				</div>
			</div>
		</div>

	</form>
</body>
</html>