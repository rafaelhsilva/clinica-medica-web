package dao;


import to.AcessoTO;

public interface AcessoDAO {

	public AcessoTO exists(AcessoTO acesso);
	public Boolean insert(String login, String senha);
}
