package dao;

import java.util.List;
import to.FuncionarioTO;

public interface FuncionarioDAO {
	
	public boolean insert(FuncionarioTO funcionario);
	public boolean delete(int id);
	public boolean update(FuncionarioTO funcionario);
	public List<FuncionarioTO> getList(FuncionarioTO func);

}
