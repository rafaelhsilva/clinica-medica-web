package dao;

import java.util.List;

import to.PacienteTO;
import to.TipoTelefoneTO;

public interface PacienteDAO {
	public boolean insert(PacienteTO paciente);
	public boolean delete(int id);
	public boolean update(PacienteTO paciente);
	public List<PacienteTO> getList(PacienteTO paciente);
	public List<TipoTelefoneTO> getTiposTelefone();
	public PacienteTO getById(Integer id);
}
