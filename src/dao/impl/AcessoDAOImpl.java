package dao.impl;

import java.sql.SQLException;

import connection.Connection;
import connection.MyPreparedStatement;
import connection.MyResultSet;
import to.AcessoTO;
import dao.AcessoDAO;

public class AcessoDAOImpl implements AcessoDAO{
	MyPreparedStatement bd = Connection.getConnection();
	
	private static final String SELECT_ACESSO = "SELECT A.Id, "
												 + " A.Login,"
												 + " Senha "
												 + "FROM Acesso A "
												 + " JOIN Pacientes P ON P.Id = A.Id "
												 + "WHERE P.Id !=0 AND P.Ativo='S'";
	
	private static final String INSERT_ACESSO =  " INSERT INTO"
												 + " Acesso(Login,Senha) "
												 + " VALUES(?,?) ";
	@Override
	public AcessoTO exists(AcessoTO acesso) {
		String sql = SELECT_ACESSO;
		AcessoTO access = new AcessoTO();
		if (acesso.getLogin() != null && acesso.getSenha() != null ){
			sql += " AND Login = ? AND Senha = ?";
	        try {
	        	bd.prepareStatement(sql);	
				bd.setString(1, acesso.getLogin());
		        bd.setString(2, acesso.getSenha());
		        MyResultSet rs = bd.executeQuery();
		        if (rs.next()){
		        	access = new AcessoTO(rs.getInt("Id"),rs.getString("Login"),rs.getString("Senha"));
		        }

		        return access;
		        
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return access;
	}
	@Override
	public Boolean insert(String login, String senha){
		String sql = INSERT_ACESSO;
        try {
        	bd.prepareStatement(sql);	
			bd.setString(1, login);
	        bd.setString(2,senha);
	        bd.executeUpdate();
	        bd.commit();
	        return true;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	return false;
	}

}
