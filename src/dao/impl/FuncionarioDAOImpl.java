package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import to.CargoTO;
import to.FuncionarioTO;
import connection.Connection;
import connection.MyPreparedStatement;
import dao.FuncionarioDAO;

public class FuncionarioDAOImpl implements FuncionarioDAO {
	MyPreparedStatement bd = Connection.getConnection();

	private static final String SELECT_FUNCIONARIO = "SELECT F.Id"
												+ " ,F.Cargo as CargoId"
												+ " ,F.Nome, "
												+ "  C.Nome as Cargo "
												+ "  FROM Funcionarios F"
												+ "  JOIN Cargos C ON C.Id = F.Cargo ";
	private static final String UPDATE_FUNCIONARIO = "UPDATE Funcionarios "
												  + " SET Nome = ?, Cargo = ? WHERE Id = ?";
	
	private static final String INACTIVATE_FUNCIONARIO = "UPDATE Funcionarios SET Ativo=0 WHERE Id = ?";
	
	private static final String INSERT_FUNCIONARIO = "INSERT INTO Funcionarios(Cargo,Nome)"
													+ " VALUES (?,?)";
	
	@Override
	public boolean insert(FuncionarioTO funcionario) {

		String sql = INSERT_FUNCIONARIO;
	        try {
	        	bd.prepareStatement(sql);	
				bd.setInt(1, funcionario.getCargo().getId());
		        bd.setString(2, funcionario.getNome());
		        bd.executeQuery();
		        
		        return true;
		        
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		return false;
	}

	@Override
	public boolean delete(int id) {
		String sql = INACTIVATE_FUNCIONARIO;
        try {
        	bd.prepareStatement(sql);	
			bd.setInt(1, id);
	        bd.executeQuery();
	        
	        return true;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return false;
	}

	@Override
	public boolean update(FuncionarioTO funcionario) {
		String sql = UPDATE_FUNCIONARIO;
        try {
        	bd.prepareStatement(sql);	
			bd.setInt(1, funcionario.getCargo().getId());
	        bd.setString(2, funcionario.getNome());
	        bd.setInt(2, funcionario.getId());
	        bd.executeQuery();
	        
	        return true;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	return false;
	}

	@Override
	public List<FuncionarioTO> getList(FuncionarioTO func) {
		String sql = SELECT_FUNCIONARIO ;
		List<FuncionarioTO> list = new ArrayList<FuncionarioTO>();
		CargoTO cargo;
        try {
        	sql += filtrosql(func, bd);
        	bd.prepareStatement(sql);	
	        ResultSet rs = bd.executeQuery(sql);
	        
	        while (rs.next()){
	        	cargo = new CargoTO(rs.getInt("CargoId"),rs.getString("Cargo"));
	        	list.add(new FuncionarioTO(rs.getInt("Id"), cargo, rs.getString("Nome"),rs.getBoolean("ativo")));	        	
	        }
	        
	        
	        return list;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return list;
	}

	private String filtrosql(FuncionarioTO func,MyPreparedStatement bd) throws SQLException {
		String sql = " where F.Id > 0 ";
		if (func.getCargo().getId() != null && func.getCargo().getId() != 0){
			sql += " AND F.Cargo = ?";
			bd.setInt(1, func.getCargo().getId());			
		}
		
		if (func.getNome() != null && func.getNome() != "") {
			sql += " AND F.Nome LIKE ?";
			bd.setString(1, "%"+func.getNome()+"%");	
		}
		
		return sql;
	}
	
}
	


