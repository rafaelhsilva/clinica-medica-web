package dao.impl;

import factory.AcessoFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import connection.Connection;
import connection.MyPreparedStatement;
import connection.MyResultSet;
import to.AcessoTO;
import to.CargoTO;
import to.FuncionarioTO;
import to.PacienteTO;
import to.TelefoneTO;
import to.TipoTelefoneTO;
import util.Generator;
import dao.PacienteDAO;

public class PacienteDAOImpl implements PacienteDAO {
	MyPreparedStatement bd = Connection.getConnection();
	
	private static final String SELECT_PACIENTE = "SELECT P.Id"
													+ " ,P.Nome "
													+ " ,P.CEP"
													+ " ,P.Numero"
													+ " ,P.Complemento"
													+ " ,P.CPF"
													+ " ,P.RG"
													+ " ,T.Area"  //Area,Numero,IdTipo
													+ " ,T.Numero"
													+ " ,T.IdTipo"
													+ " ,Tipo.Nome"
													+ " ,A.Login"
													+ " ,A.Senha"
													+ " ,P.Ativo"
													+ "  FROM Pacientes P"
													+ "  JOIN Telefones T ON T.IdPaciente = P.Id "
													+ "  JOIN TiposDeTelefone Tipo ON Tipo.Id = T.IdTipo "
													+ "  JOIN Acesso A ON A.Id = P.Id"
													+ "  WHERE P.Ativo = 'S'";
	private static final String UPDATE_PACIENTE = "UPDATE Pacientes "
													+ " SET Nome = ?, "
													+ "CEP = ?, "
													+ "Numero = ?, "
													+ "Complemento = ?, "
													+ "RG = ?, "
													+ "CPF = ? "
													+ "WHERE Id = ?";

	private static final String INACTIVATE_PACIENTE = "UPDATE Pacientes SET Ativo='N' WHERE Id = ?";
	
	private static final String SELECT_PHONES_TYPE = "SELECT Id,Nome FROM TiposDeTelefone";

	private static final String INSERT_PACIENTE = "INSERT INTO Pacientes(Id,Nome,CEP,Numero,Complemento,RG,CPF,Ativo)"
														+ " VALUES (?,?,?,?,?,?,?,?)";
	private static final String INSERT_PHONE = "INSERT INTO Telefones(Area,Numero,IdTipo,IdPaciente)"
			+ " VALUES (?,?,?,?)";	
	
	private static final String UPDATE_PHONE = "UPDATE Telefones SET Area=?, Numero = ?, IdTipo = ? WHERE IdPaciente= ?";
	private static final String UPDATE_ACCESS = "UPDATE Acesso SET Login = ?, Senha = ? WHERE Id= ?";
	
	
	@Override
	public boolean insert(PacienteTO paciente) {
		String sql = "";
        try {
   
	        AcessoFactory access = new AcessoFactory();
			String login = Generator.gerarNovoLogin();
			String senha = Generator.gerarNovaSenha();
			access.insert(login,senha);

            String sqlId = "SELECT id FROM Acesso WHERE Login LIKE '" + login
                    + "' AND Senha LIKE '" + senha + "'";

            sql = sqlId;
            bd.prepareStatement(sql);
            MyResultSet resultado = bd.executeQuery();
            resultado.next();
            paciente.setId(resultado.getInt("Id"));
            
            sql = INSERT_PACIENTE;
            bd.prepareStatement(sql);
			bd.setInt(1, paciente.getId() );
	        bd.setString(2, paciente.getNome());
	        bd.setInt(3, paciente.getCep() );
	        bd.setInt(4, paciente.getNumero() );
	        bd.setString(5, paciente.getComplemento() );
	        bd.setString(6, paciente.getRg());
	        bd.setString(7, paciente.getCpf());
	        bd.setString(8, "S");
	        bd.executeUpdate();
	        bd.commit();
	        insertTelefone(paciente);
	        
	        return true;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return false;
	}
	
	private void insertTelefone(PacienteTO paciente){
		String sql = INSERT_PHONE;
        try {
        	bd.prepareStatement(sql);	
			bd.setInt(1, paciente.getTelefone().getArea());
	        bd.setInt(2, paciente.getTelefone().getNumero());
	        bd.setInt(3, paciente.getTelefone().getTipoTelefone().getId());
	        bd.setInt(4, paciente.getId());
	        bd.executeUpdate();
	        bd.commit();
	        
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean updateTelefone(PacienteTO paciente) {
		String sql = UPDATE_PHONE;
        try {
        	bd.prepareStatement(sql);	
			bd.setInt(1, paciente.getTelefone().getArea());
	        bd.setInt(2, paciente.getTelefone().getNumero());
	        bd.setInt(3, paciente.getTelefone().getTipoTelefone().getId());
	        bd.setInt(4, paciente.getId());
	        bd.executeUpdate();
	        bd.commit();
	        
	        return true;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return false;
	}
	
	public boolean updateAcesso(AcessoTO acesso) {
		String sql = UPDATE_ACCESS;
        try {
        	bd.prepareStatement(sql);	
			bd.setString(1, acesso.getLogin());
	        bd.setString(2, acesso.getSenha());
	        bd.setInt(3, acesso.getId());
	        bd.executeUpdate();
	        bd.commit();
	        
	        return true;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return false;
	}

	@Override
	public boolean delete(int id) {
		String sql = INACTIVATE_PACIENTE;
        try {
        	bd.prepareStatement(sql);	
			bd.setInt(1, id);
	        bd.executeUpdate();
	        bd.commit();	        
	        return true;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return false;
	}

	@Override
	public boolean update(PacienteTO paciente) {
		String sql = UPDATE_PACIENTE;
        try {
        	bd.prepareStatement(sql);	
			bd.setString(1, paciente.getNome());
	        bd.setInt(2, paciente.getCep());
	        bd.setInt(3, paciente.getNumero());
	        bd.setString(4, paciente.getComplemento());
	        bd.setString(5, paciente.getRg());
	        bd.setString(6, paciente.getCpf());
	        bd.setInt(7, paciente.getId());
	        bd.executeUpdate();
	        bd.commit();
	        
	        updateTelefone(paciente);
	        updateAcesso(new AcessoTO(paciente.getId(),paciente.getAcesso().getLogin(),paciente.getAcesso().getSenha()));
	        return true;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return false;
	}

	@Override
	public List<PacienteTO> getList(PacienteTO paciente) {
		String sql = SELECT_PACIENTE;
		List<PacienteTO> list = new ArrayList<PacienteTO>();
        try {
        	bd.prepareStatement(sql);	
        	MyResultSet rs = bd.executeQuery();
	        
	        if (rs!=null) {
		        while (rs.next()){
		        	TipoTelefoneTO tt = new TipoTelefoneTO(rs.getInt("IdTipo"),rs.getString("Nome"));
		        	TelefoneTO tel = new TelefoneTO(rs.getInt("Area"),rs.getInt("Numero"),tt, rs.getInt("Id"));
		        	AcessoTO ac = new AcessoTO(rs.getString("login"),rs.getString("senha"));
		        	list.add(new PacienteTO(rs.getInt("Id"), rs.getString("Nome"), rs.getInt("CEP"),
		        			rs.getInt("Numero"),rs.getString("Complemento"),
		        			rs.getString("RG"),rs.getString("CPF"),rs.getString("Ativo"),tel,ac));	 
		        }
	        
	        }
	        return list;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return list;
	}

	private String filtrosql(PacienteTO paciente, MyPreparedStatement bd2) throws SQLException {
		String sql = " where Id > 0 ";
		if (paciente.getCep() != null && paciente.getCep() != 0){
			sql += " AND F.CEP = ?";
			bd.setInt(1, paciente.getCep());			
		}
		
		if (paciente.getNome() != null && paciente.getNome() != "") {
			sql += " AND Nome LIKE ?";
			bd.setString(1, "%"+paciente.getNome()+"%");	
		}
		
		return sql;
	}

	@Override
	public List<TipoTelefoneTO> getTiposTelefone() {
		String sql = SELECT_PHONES_TYPE;
		List<TipoTelefoneTO> list = new ArrayList<TipoTelefoneTO>();
        try {
        	bd.prepareStatement(sql);	
        	MyResultSet rs = bd.executeQuery();
	        
	        while (rs.next()){
	        	list.add(new TipoTelefoneTO(rs.getInt("Id"), rs.getString("Nome")));        	
	        }
	        
	        
	        return list;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return list;
	}
	
	@Override
	public PacienteTO getById(Integer id) {
		String sql = SELECT_PACIENTE;
		PacienteTO paciente = new PacienteTO();
        try {
        	sql += " AND P.Id = ? ";
        	bd.prepareStatement(sql);	
        	bd.setInt(1, id);
        	MyResultSet rs = bd.executeQuery();
	        
	        if (rs!=null) {
		        if (rs.next()){
		        	TipoTelefoneTO tt = new TipoTelefoneTO(rs.getInt("IdTipo"),rs.getString("Nome"));
		        	TelefoneTO tel = new TelefoneTO(rs.getInt("Area"),rs.getInt("Numero"),tt, rs.getInt("Id"));
		        	AcessoTO ac = new AcessoTO(rs.getString("login"),rs.getString("senha"));
		        	paciente  = new PacienteTO(rs.getInt("Id"), rs.getString("Nome"), rs.getInt("CEP"),
		        			rs.getInt("Numero"),rs.getString("Complemento"),
		        			rs.getString("RG"),rs.getString("CPF"),rs.getString("Ativo"),tel,ac);		        	
		        }
	        
	        }
	        return paciente;
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
        return paciente;
	}
	

}
