package factory;

import dao.impl.AcessoDAOImpl;
import to.AcessoTO;

public class AcessoFactory {

	AcessoDAOImpl access = new AcessoDAOImpl();
	public AcessoTO exists(AcessoTO acesso){
		return access.exists(acesso);
	}
	
	public Boolean insert(String login, String senha){
		return access.insert(login,senha);
	}
}
