package factory;

import java.util.List;

import to.PacienteTO;
import to.TipoTelefoneTO;
import dao.impl.PacienteDAOImpl;

public class PacienteFactory{
	PacienteDAOImpl dao = new PacienteDAOImpl();
	
	public boolean insert(PacienteTO paciente) {
		return dao.insert(paciente);
	}

	public boolean delete(int id) {
		return dao.delete(id);
	}

	public boolean update(PacienteTO paciente) {
		return dao.update(paciente);
	}

	public List<PacienteTO> getList(PacienteTO paciente) {
		return dao.getList(paciente);
	}
	public PacienteTO getById(Integer id) {
		return dao.getById(id);
	}
	
	public List<TipoTelefoneTO> getTiposTelefone() {
		return dao.getTiposTelefone();
	}

}
