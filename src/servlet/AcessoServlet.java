package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import factory.AcessoFactory;
import to.AcessoTO;

/**
 * Servlet implementation class AcessoFactory
 */
@WebServlet("/AcessoServlet")
public class AcessoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AcessoFactory accessFactory = new AcessoFactory();
    /**
     * Default constructor. 
     */
    public AcessoServlet() {
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("login");
		String senha = request.getParameter("password");
		AcessoTO ac = accessFactory.exists(new AcessoTO(login,senha));
		if (ac.getId() != null){
			HttpSession session = request.getSession();
			session.setAttribute("pacienteId", ac.getId());
			response.sendRedirect("Menu.jsp");
		}
		else {
			response.sendRedirect("Error.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
