package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import to.*;
import factory.PacienteFactory;

/**
 * Servlet implementation class PacienteServlet
 */
@WebServlet("/PacienteServlet")
public class PacienteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      PacienteFactory pac = new PacienteFactory();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PacienteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
			
		PacienteTO paciente = new PacienteTO();
		AcessoTO acesso = new AcessoTO();
		TelefoneTO tel = new TelefoneTO();
		String nome = request.getParameter("nomePaciente");
		String cpf= request.getParameter("cpfPaciente");
		String rg = request.getParameter("rgPaciente");
		Integer cep = Integer.valueOf(request.getParameter("cepPaciente"));
		Integer numero = Integer.valueOf(request.getParameter("numPaciente"));
		String complemento = request.getParameter("complPaciente");
		Integer tipoTelefone =  Integer.valueOf(request.getParameter("tipoTelPaciente"));
		Integer telefone =  Integer.valueOf(request.getParameter("telPaciente"));
		Integer area =  Integer.valueOf(request.getParameter("areaTelPaciente"));
		
	
		if (request.getParameter("loginPaciente") != null && request.getParameter("senhaPaciente") != null){
			acesso.setLogin(request.getParameter("loginPaciente"));
			acesso.setSenha(request.getParameter("senhaPaciente"));
			paciente.setAcesso(acesso);
		}
		
		paciente.setNome(nome);
		paciente.setCep(cep);
		paciente.setNumero(numero);
		paciente.setCpf(cpf);
		paciente.setRg(rg);
		paciente.setComplemento(complemento);
		tel.setArea(area);
		tel.setNumero(telefone);
		TipoTelefoneTO tt = new TipoTelefoneTO();
		tt.setId(tipoTelefone);
		tel.setTipoTelefone(tt);
		paciente.setTelefone(tel);
		if (request.getParameter("idPaciente") != null){
			paciente.setId(Integer.parseInt(request.getParameter("idPaciente")));
			
		}
		
		if (paciente.getId() != null){
			pac.update(paciente);
		}
		else {
			pac.insert(paciente);
		}	
		response.sendRedirect("ListPaciente.jsp");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);

	}

}
