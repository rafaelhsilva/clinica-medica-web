package to;

public class AcessoTO {
	
	private Integer id;
	private String login;
	private String senha;
	
	public AcessoTO(){
		this.login = "";
		this.senha = "";
	}
	public AcessoTO(Integer id, String l, String s){
		setId(id);
		setLogin(l);
		setSenha(s);
	}
	
	public AcessoTO(String l, String s){
		setLogin(l);
		setSenha(s);
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}

}
