package to;

public class CargoTO {
    private Integer id;
    private String nome;

    public CargoTO(){
    	this.id = 0;
    	this.nome = "";
    }
    public CargoTO(Integer id, String nome){
    	this.id = id;
    	this.nome = nome;
    }   
    
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}
