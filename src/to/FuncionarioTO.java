package to;

public class FuncionarioTO {
	
	private Integer id;
	private CargoTO cargo;
	private String nome;
	private Boolean ativo;
	
	public FuncionarioTO(){
		this.id = 0;
		this.cargo = new CargoTO();
		this.nome = "";
		this.ativo = false;
	}
	
	public FuncionarioTO(Integer id, CargoTO cargo, String nome,Boolean ativo){
		this.id = id;
		this.cargo = cargo;
		this.nome = nome;
		this.ativo = ativo;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public CargoTO getCargo() {
		return cargo;
	}
	public void setCargo(CargoTO cargo) {
		this.cargo = cargo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
