package to;

public class PacienteTO {
	private Integer id;
	private String nome;
	private Integer cep;
	private Integer numero;
	private String complemento;
	private String rg;
	private String cpf;
	private String ativo;
	private TelefoneTO telefone;
	private AcessoTO acesso;
	
	public PacienteTO(){
		this.id = 0;
		this.nome = "";
		this.cep = 0;
		this.numero= 0;
		this.complemento = "";		
		this.rg= "";	
		this.cpf= "";	
		this.ativo= "S";
		setAcesso(new AcessoTO());
		setTelefone(new TelefoneTO());
	}
	
	public PacienteTO(Integer id,String nome,Integer cep,Integer numero,String complemento,String rg, String cpf, String ativo, TelefoneTO tel,AcessoTO ac){
		setId(id);
		setNome(nome);
		setCep(cep);
		setNumero(numero);
		setComplemento(complemento);
		setRg(rg);
		setCpf(cpf);
		setAtivo(ativo);
		setTelefone(tel);
		setAcesso(ac);
	}
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCep() {
		return cep;
	}

	public void setCep(Integer cep) {
		this.cep = cep;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String isAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	public TelefoneTO getTelefone() {
		return telefone;
	}

	public void setTelefone(TelefoneTO telefone) {
		this.telefone = telefone;
	}

	public AcessoTO getAcesso() {
		return acesso;
	}

	public void setAcesso(AcessoTO acesso) {
		this.acesso = acesso;
	}

}
