package to;

public class TelefoneTO {

	private Integer area;
	private Integer numero;
	private TipoTelefoneTO tipoTelefone;
	private Integer pacienteId;
	
	
	public TelefoneTO(){
		setArea(0);
		setNumero(0);
		setTipoTelefone(new TipoTelefoneTO());
		setPacienteId(0);
	}
	
	public TelefoneTO(Integer area, Integer num, TipoTelefoneTO tt, Integer pacId){
		setArea(area);
		setNumero(num);
		setTipoTelefone(tt);
		setPacienteId(pacId);
	}
	
	public Integer getArea() {
		return area;
	}
	public void setArea(Integer area) {
		this.area = area;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getPacienteId() {
		return pacienteId;
	}
	public void setPacienteId(Integer pacienteId) {
		this.pacienteId = pacienteId;
	}
	public TipoTelefoneTO getTipoTelefone() {
		return tipoTelefone;
	}
	public void setTipoTelefone(TipoTelefoneTO tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}
}
