package to;

public class TipoTelefoneTO {

	private Integer id;
	private String nome;
	
	public TipoTelefoneTO(){
		setId(0);
		setNome("");
	}
	public TipoTelefoneTO(Integer id, String nome){
		setId(id);
		setNome(nome);
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
