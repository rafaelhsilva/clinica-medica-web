package util;

import java.util.Random;

public class Generator {
	private static final Integer MAX = 8;
	private static final Integer MIN = 0;

	public static String gerarNovaSenha(){
		Random random = new Random();
		String ret = random.nextInt(100000) * (MAX- MIN) +"";
		return ret;		
	}
	
	public static String gerarNovoLogin(){
		Random random = new Random();
		
		return "PAC" + random.nextInt(100000) *(MAX- MIN);		
	}
}
